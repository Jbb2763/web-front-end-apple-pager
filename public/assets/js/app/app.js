'use strict';

angular.module('pagerApp', [
  //'ngRoute',
  'pagerApp.dashboard'
])
  .filter('floor', function() {
    return function(input) {
      return Math.floor(input);
    };
  })
  .config(['$httpProvider', '$locationProvider', '$sceProvider',
    function config($httpProvider, $locationProvider, $sceProvider) {
      $locationProvider.html5Mode({
        enabled: true,
        requireBase: false,
        rewriteLinks: false
      });
      $sceProvider.enabled(false);
    }
  ])

  .run(['$rootScope', '$location', function($rootScope, $location){

  }]);
angular.module('pagerApp.dashboard', [])
  .controller('DashboardCtrl', [
    '$scope', '$window', '$q', '$location', '$interval',
    function($scope, $window, $q, $location, $interval) {

    $scope.rooms = [
      {
        name: 'Room 1',
        status: 'empty',
        time: 50,
        maxTime: 2000
      },
      {
        name: 'Room 2',
        status: 'pending',
        time: 1170,
        maxTime: 1200
      },
      {
        name: 'Room 3',
        status: 'ready',
        time: 1200,
        maxTime: 3600
      },
      {
        name: 'Room 4',
        status: 'empty',
        time: 50,
        maxTime: 2000
      },
      {
        name: 'Room 5',
        status: 'pending',
        time: 1170,
        maxTime: 1200
      },
      {
        name: 'Room 6',
        status: 'ready',
        time: 1200,
        maxTime: 3600
      },
      {
        name: 'Room 7',
        status: 'empty',
        time: 50,
        maxTime: 2000
      },
      {
        name: 'Room 8',
        status: 'pending',
        time: 1170,
        maxTime: 1200
      },
      {
        name: 'Room 9',
        status: 'ready',
        time: 1200,
        maxTime: 3600
      }
    ];



    function setCirclePercent(id, percent, status){
      var color = '#39D661'; //green
      if(status == 'pending'){
        color = '#FFB100';
      } else if(status == 'empty'){
        color = '#ABABAB';
      }
      var circle = document.getElementById(id);

      if(percent <= 50){
        var degrees = 90 + ( 360 * (percent/100) )
        var style = 'linear-gradient('+degrees+'deg, transparent 50%, #f8f8f8 50%), linear-gradient(90deg, #f8f8f8 50%, transparent 50%)';
      } else {
        var degrees = 90 + ( 180 * ((percent-50)/50) );
        // degrees = 270;
        var style = 'linear-gradient('+degrees+'deg, transparent 50%, '+color+' 50%), linear-gradient(90deg, #f8f8f8 50%, transparent 50%)';
      }

      circle.style.backgroundImage = style;
    }

    var interval = $interval(function(){
      for(var i = 0; i < $scope.rooms.length; i++){
        if($scope.rooms[i].status != 'empty'){
          setCirclePercent('circle'+i, $scope.rooms[i].time/$scope.rooms[i].maxTime*100, $scope.rooms[i].status)
          $scope.rooms[i].time += 3;
          if($scope.rooms[i].time > $scope.rooms[i].maxTime){
            $scope.rooms[i].time = 0;
          }
        } else if($scope.rooms[i].time != 0){
          $scope.rooms[i].time = 0;
          setCirclePercent('circle'+i, 100, $scope.rooms[i].status)
        }
      }
    }, 100);

    $scope.changeStatus = function(index, status){
      if($scope.rooms[index].status == 'empty' && status != 'empty') {
        var circle = document.getElementById('circle' + index);
        var emptyStyle = 'linear-gradient(90deg, transparent 50%, #f8f8f8 50%), linear-gradient(90deg, #f8f8f8 50%, transparent 50%)';
        circle.style.backgroundImage = emptyStyle;
      }
      $scope.rooms[index].time = 0;
      $scope.rooms[index].status = status;
    }

    $scope.$on('$destroy', function() {

      // Make sure that the interval is destroyed too
      $interval.cancel(interval);
    });
}]);

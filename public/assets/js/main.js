window.addEventListener('load', function() {

	var circle = document.getElementById('circle');

	var percent = 0;

	var greenColor = '#39D661';

	function setCirclePercent(percent){
		if(percent <= 50){
			var degrees = 90 + ( 360 * (percent/100) )
	    	var style = 'linear-gradient('+degrees+'deg, transparent 50%, #f8f8f8 50%), linear-gradient(90deg, #f8f8f8 50%, transparent 50%)';
	    } else {
			var degrees = 90 + ( 180 * ((percent-50)/50) );
			// degrees = 270;
			var style = 'linear-gradient('+degrees+'deg, transparent 50%, '+greenColor+' 50%), linear-gradient(90deg, #f8f8f8 50%, transparent 50%)';
	    }
	    
	    circle.style.backgroundImage = style;
	}

	// setInterval(function(){
	// 	percent += 0.5;
	//
	// 	if(percent >= 100){
	// 		percent = 0;
	// 	}
  //
	//    	setCirclePercent(percent);
  //
	// },1000);
});
